.PHONY: clean test install dependencies run

build: dependencies
	go build

dependencies:

docs:
	@echo "Generating PDF at ./api/Glyph-1.0.pdf"
	@pdflatex --output-dir=./api ./api/src/Glyph-1-0.tex || echo "pdflatex is required to compile Glyph documentation!"
	
	@echo "Cleaning output..."
	@rm ./api/Glyph-1-0.aux
	@rm ./api/Glyph-1-0.log
	@rm ./api/Glyph-1-0.out
	@rm ./api/Glyph-1-0.toc
	@rm ./api/Glyph-1-0.lg
	@rm ./api/Glyph-1-0.idv