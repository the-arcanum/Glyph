# Glyph

Glyph is a protocol created on top of TCP (with SSL support). It is designed to be a simple way to transfer both text and binary data with low overhead.

Originally, it was designed for use with the Arcanum family of programs, but is free to use anywhere, provided the license terms are followed.

This repository contains both the Glyph API specification documents *and* an example library for it, written in Go.

## The API Spec

The API Specification is available in markdown in the [Specifications and API Subdirectory of this Repo](api/)

## The Library

To download:

```bash
go get -u gitlab.com/the-arcanum/glyph
```

To use:

```go
import "gitlab.com/the-arcanum/glyph"
```

The library functions are 100% compliant with the API specification.

## Usage

In the spirit of simplicity, the set of methods implementing the API is very small.

Opening a glyph connection:

```go
    conn, err := glyph.New("127.0.0.1:5555")

    /*  If you have the "net" package (go standard library) imported, 
        you may also use a net.TCPAddr as the argument */
    addr, err := net.ResolveTCPAddr("tcp", "127.0.0.1:5555")
    conn, err := glyph.NewFrom(addr)
```

Sending raw data:

```go
    data := []byte{ 255, 234, 0, 12, 52, 56 }
    err := conn.Send(data)
```

Waiting for data:

```go
    // Data is returned in the form of a byte slice.
    data, err := conn.Expect()
```